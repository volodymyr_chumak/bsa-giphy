package com.bsa.giphy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Gif {
    private String id;
    private String url;
}
