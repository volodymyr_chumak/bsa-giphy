package com.bsa.giphy.service;

import com.bsa.giphy.entity.Gif;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Optional;

@Component
public class HttpGiphyApiClient {
    private static final Logger logger = LoggerFactory.getLogger(HttpGiphyApiClient.class);

    @Value("${api.giphy-url}")
    private String giphyApiUrl;

    @Value("${api.giphy-key}")
    private String giphyApiKey;

    private final HttpClient client;

    @Autowired
    public HttpGiphyApiClient(HttpClient client) {
        this.client = client;
    }

    public Optional<Gif> getGif(String query) {
        try {
            var response = client.send(buildGetRequest(query), HttpResponse.BodyHandlers.ofString());

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(response.body());
            } catch (JSONException ex){
                logger.error(ex.getMessage(), ex);
            }
            return Optional.of(jsonObjectToGif(jsonObject));
        } catch (IOException | InterruptedException ex) {
            logger.error(ex.getMessage(), ex);
            return Optional.empty();
        }
    }

    private HttpRequest buildGetRequest(String query) {
        final String request = giphyApiUrl + "?api_key=" + giphyApiKey + "&tag=" + query + "&limit=1";

        return HttpRequest
                .newBuilder()
                .uri(URI.create(request))
                .GET()
                .build();
    }

    private Gif jsonObjectToGif(JSONObject jsonObject) {
        JSONObject data = jsonObject.getJSONObject("data");

        String id = data.getString("id");
        String original_url = data.getString("image_original_url");
        String url = original_url.replaceFirst("media[0-9]+", "i");

        return new Gif(id, url);
    }
}
