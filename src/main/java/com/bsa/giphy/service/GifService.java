package com.bsa.giphy.service;

import com.bsa.giphy.dto.CacheDto;
import com.bsa.giphy.repository.CacheRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GifService {
    private final CacheRepository cacheRepository;

    @Autowired
    public GifService(CacheRepository cacheRepository) {
        this.cacheRepository = cacheRepository;
    }

    public List<String> getPaths() {
        List<CacheDto> cacheFromDrive = cacheRepository.getAllCacheFromDrive();
        List<String> paths = new ArrayList<>();

        cacheFromDrive.stream()
                .map(CacheDto::getGifs)
                .forEach(paths::addAll);

        return paths;
    }
}
