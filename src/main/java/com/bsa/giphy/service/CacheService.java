package com.bsa.giphy.service;

import com.bsa.giphy.dto.CacheDto;
import com.bsa.giphy.entity.Gif;
import com.bsa.giphy.repository.CacheRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CacheService {

    private final CacheRepository cacheRepository;

    private final HttpGiphyApiClient httpGiphyApiClient;

    @Autowired
    public CacheService(CacheRepository cacheRepository, HttpGiphyApiClient httpGiphyApiClient) {
        this.cacheRepository = cacheRepository;
        this.httpGiphyApiClient = httpGiphyApiClient;
    }

    public List<CacheDto> getFromDrive(String query) {
        List<CacheDto> cacheFromDrive = cacheRepository.getAllCacheFromDrive();

        if (query != null) {
            cacheFromDrive = cacheFromDrive.stream()
                    .filter(cacheDto -> cacheDto.getQuery().equals(query))
                    .collect(Collectors.toList());
        }

        return cacheFromDrive;
    }

    public CacheDto downloadGif(String query) {
        Optional<Gif> gifFromGiphy = httpGiphyApiClient.getGif(query);
        String gifPath = cacheRepository.addGifToCacheFolder(gifFromGiphy.get(), query);

        List<CacheDto> cacheFromDrive = cacheRepository.getAllCacheFromDrive().stream()
                    .filter(cacheDto -> cacheDto.getQuery().equals(query))
                    .collect(Collectors.toList());


        return cacheFromDrive.get(0);
    }

    public void cleanCacheFolder() {
        cacheRepository.cleanCacheFolder();
    }
}
