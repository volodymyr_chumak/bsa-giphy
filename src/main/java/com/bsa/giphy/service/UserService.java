package com.bsa.giphy.service;

import com.bsa.giphy.dto.CacheDto;
import com.bsa.giphy.dto.GifFromDrive;
import com.bsa.giphy.dto.HistoryDto;
import com.bsa.giphy.entity.Gif;
import com.bsa.giphy.exception.UserOrHistoryNotFoundException;
import com.bsa.giphy.repository.CacheRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final CacheRepository cacheRepository;

    private final HttpGiphyApiClient httpGiphyApiClient;

    @Autowired
    public UserService(CacheRepository cacheRepository, HttpGiphyApiClient httpGiphyApiClient) {
        this.cacheRepository = cacheRepository;
        this.httpGiphyApiClient = httpGiphyApiClient;
    }

    public Optional<List<CacheDto>> getAllFiles(String id) {
        return cacheRepository.getCacheFromUserFolder(id);
    }

    public Optional<List<HistoryDto>> getHistory(String id) {
        return cacheRepository.getUserHistory(id);
    }

    public void cleanHistory(String id) throws UserOrHistoryNotFoundException {
        cacheRepository.cleanHistoryFile(id);
    }

    public Optional<String> searchGif(String id, String query, String force) {
        if (!force.equals("true")) {
            Optional<String> gifPathFromMemory = cacheRepository.getGifPath(id, query);
            if (gifPathFromMemory.isPresent()) return gifPathFromMemory;
        }

        return searchGifOnDisk(id, query);
    }

    private Optional<String> searchGifOnDisk(String id, String query) {
        Optional<String> gifPathFromCacheFolder = cacheRepository.getGifPathFromCacheFolder(id, query);
        if (gifPathFromCacheFolder.isEmpty()) return Optional.empty();
        cacheRepository.update(id, query);

        return cacheRepository.getGifPath(id, query);
    }

    public Optional<String> generateGif(String id, String query, String force) {
        if (!force.equals("true")) {
            Optional<GifFromDrive> gif = cacheRepository.getGifFromCacheForlder(query);
            if (gif.isPresent()) {
                String gifPath = addGifToUser(id, query, gif.get());
                return Optional.of(gifPath);
            }
        }

        return searchOnGiphy(id, query);
    }

    private Optional<String> searchOnGiphy(String id, String query) {
        Optional<Gif> gifFromGiphy = httpGiphyApiClient.getGif(query);
        if (gifFromGiphy.isEmpty()) return Optional.empty();

        cacheRepository.addGifToCacheFolder(gifFromGiphy.get(), query);
        Optional<GifFromDrive> gif = cacheRepository.getGifFromCacheForlder(query);
        String gifPath = addGifToUser(id, query, gif.get());

        return Optional.of(gifPath);
    }

    public String addGifToUser(String id, String query, GifFromDrive gif) {
        String gifPath = cacheRepository.addGifToUser(id, query, gif);
        cacheRepository.update(id, query);
        return gifPath;
    }

    public void cleanUserCacheFromMemoryByQuery(String id, String query) {
        cacheRepository.cleanUserCacheFromMemoryByQuery(id, query);
    }

    public void cleanAllUserCacheFromMemory(String id) {
        cacheRepository.cleanAllUserCacheFromMemory(id);
    }

    public void cleanAllUserData(String id) {
        cacheRepository.cleanAllUserData(id);
    }
}
