package com.bsa.giphy.repository;

import com.bsa.giphy.dto.CacheDto;
import com.bsa.giphy.dto.CsvHistoryDto;
import com.bsa.giphy.dto.GifFromDrive;
import com.bsa.giphy.dto.HistoryDto;
import com.bsa.giphy.entity.Gif;
import com.bsa.giphy.exception.UserOrHistoryNotFoundException;
import com.opencsv.bean.CsvToBeanBuilder;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class CacheRepository {
    private Map<String, Map<String, List<String>>> cache = new HashMap<>();

    @Value("${bsa_giphy.path}")
    private String bsa_giphy;

    @Value("${cache.path}")
    private String pathToCacheFolder;

    public Optional<GifFromDrive> getGifFromCacheForlder(String query) {
        createFolderIfTheyNotExist(bsa_giphy);
        createFolderIfTheyNotExist(pathToCacheFolder);

        File queryFolder = new File(pathToCacheFolder + "/" + query);
        if (!queryFolder.exists()) {
            queryFolder.mkdir();
            return Optional.empty();
        }

        Optional<File> gifOptional = getRandomGifFile(queryFolder);
        if (gifOptional.isEmpty()) return Optional.empty();

        GifFromDrive gifFromDrive = fileToGifFromDriveMapper(gifOptional.get());

        return Optional.of(gifFromDrive);
    }

    private void createFolderIfTheyNotExist(String path) {
        File folder = new File(path);
        if (!folder.exists()) folder.mkdir();
    }

    private Optional<File> getRandomGifFile(File folderWithGifs) {
        File[] gifs = folderWithGifs.listFiles();
        if (gifs.length == 0) return Optional.empty();
        Random rand = new Random();
        File gif = gifs[rand.nextInt(gifs.length)];

        return Optional.of(gif);
    }

    private GifFromDrive fileToGifFromDriveMapper(File gif) {
        String gifName = gif.getName();
        String id = gifName.replaceFirst("[.][^.]+$", "");
        String path = gif.getAbsolutePath();

        return new GifFromDrive(id, path);
    }

    public String addGifToUser(String id, String query, GifFromDrive gif) {
        final String usersFolderPath = bsa_giphy + "/users";
        final String userFolderPath = bsa_giphy + "/users/" + id;
        final String queryFolderPath = bsa_giphy + "/users/" + id + "/" + query;
        final String destinationPath = queryFolderPath + "/" + gif.getId() + ".gif";

        createFolderIfTheyNotExist(usersFolderPath);
        createFolderIfTheyNotExist(userFolderPath);
        createFolderIfTheyNotExist(queryFolderPath);

        File gifFile = new File(destinationPath);
        if (!gifFile.exists()) {
            try {
                Files.copy(Path.of(gif.getPath()), Path.of(destinationPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
            log(query, destinationPath, id);
        }

        return destinationPath;
    }

    public void update(String userId, String query) {
        final String queryFolderPath = bsa_giphy + "/users/" + userId + "/" + query;
        File[] paths = new File(queryFolderPath).listFiles();

        List<String> updatedPaths = Arrays.stream(paths)
                .map(File::getAbsolutePath)
                .collect(Collectors.toList());

        if (cache.containsKey(userId)) {
            cache.get(userId).put(query, updatedPaths);
        } else {
            Map<String, List<String>> usersMap = new HashMap<>();
            usersMap.put(query, updatedPaths);
            cache.put(userId, usersMap);
        }
    }

    public Optional<String> getGifPath(String userId, String query) {
        if (isCacheMapDontContainsGif(userId, query)) return  Optional.empty();

        List<String> gifsPath = cache.get(userId).get(query);
        if (gifsPath.size() == 0) return Optional.empty();

        String gifPath = getRandomGifPath(gifsPath);

        return Optional.of(gifPath);
    }

    private boolean isCacheMapDontContainsGif(String userId, String query) {
        return !cache.containsKey(userId) || !cache.get(userId).containsKey(query);
    }

    private String getRandomGifPath(List<String> gifsPath) {
        Random rand = new Random();
        return gifsPath.get(rand.nextInt(gifsPath.size()));
    }

    public String addGifToCacheFolder(Gif gif, String query) {
        final String queryFolderPath = pathToCacheFolder + "/" + query;
        final String destinationPath = queryFolderPath + "/" + gif.getId() + ".gif";

        createFolderIfTheyNotExist(bsa_giphy);
        createFolderIfTheyNotExist(pathToCacheFolder);
        createFolderIfTheyNotExist(queryFolderPath);

        try {
            saveGif(gif.getUrl(), destinationPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return destinationPath;
    }

    public static void saveGif(String gifUrl, String destinationPath) throws IOException {
        URL url = new URL(gifUrl);
        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(destinationPath);

        byte[] b = new byte[2048];
        int length;

        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }

        is.close();
        os.close();
    }

    public Optional<String> getGifPathFromCacheFolder(String userId, String query) {
        final String gifsFolderPath = bsa_giphy + "/users/" + userId + "/" + query;

        File gifsFolder = new File(gifsFolderPath);
        if (!gifsFolder.exists()) return Optional.empty();

        File[] gifsPaths = gifsFolder.listFiles();
        if (gifsPaths.length == 0) return Optional.empty();

        Random rand = new Random();
        File gif = gifsPaths[rand.nextInt(gifsPaths.length)];

        return Optional.of(gif.getAbsolutePath());
    }

    public List<CacheDto> getAllCacheFromDrive() {
        File[] queries = new File(pathToCacheFolder).listFiles();

        return getCacheDtosFromFolder(queries, null);
    }

    public Optional<List<CacheDto>> getCacheFromUserFolder(String userId) {
        final String userFolderPath = bsa_giphy + "/users/" + userId;

        File userFolder = new File(userFolderPath);
        if (!userFolder.exists()) return Optional.empty();
        File[] queries = userFolder.listFiles(File::isDirectory);

        List<CacheDto> cacheDtos = getCacheDtosFromFolder(queries, userId);
        return Optional.of(cacheDtos);
    }

    private List<CacheDto> getCacheDtosFromFolder(File[] queries, String userId) {
        List<CacheDto> cacheDtos = new ArrayList<>();

        Arrays.stream(queries)
                .forEach(query -> {
                    String queryName = query.getName();

                    String pathToQueryFolder;
                    if (userId != null) {
                        pathToQueryFolder = bsa_giphy + "/users/" + userId + "/" + queryName;
                    } else {
                        pathToQueryFolder = bsa_giphy + "/cache/" + queryName;
                    }
                    File[] files = new File(pathToQueryFolder).listFiles();

                    List<String> paths = new ArrayList<>();
                    Arrays.stream(files).forEach(file -> paths.add(file.getAbsolutePath()));

                    cacheDtos.add(new CacheDto(queryName, paths));
                });

        return cacheDtos;
    }

    public Optional<List<HistoryDto>> getUserHistory(String userId) {
        final String userFolderPath = bsa_giphy + "/users/" + userId;

        File userFolder = new File(userFolderPath);
        if (!userFolder.exists()) return Optional.empty();

        File historyFile = new File(userFolderPath + "/history.csv");
        if (!historyFile.exists()) return Optional.empty();

        List<CsvHistoryDto> csvHistoryDtos = new ArrayList<>();
        try {
            csvHistoryDtos = new CsvToBeanBuilder(new FileReader(historyFile.getAbsolutePath()))
                    .withType(CsvHistoryDto.class)
                    .build()
                    .parse();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        List<HistoryDto> historyDtos = csvHistoryDtos.stream()
                .map(csvDto -> new HistoryDto(csvDto.getDate(), csvDto.getQuery(), csvDto.getGif()))
                .collect(Collectors.toList());

        return Optional.of(historyDtos);
    }

    public void cleanCacheFolder() {
        File cacheFolder = new File(pathToCacheFolder);
        try {
            FileUtils.cleanDirectory(cacheFolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void cleanHistoryFile(String userId) throws UserOrHistoryNotFoundException {
        final String pathToHistoryFile = bsa_giphy + "/users/" + userId + "/history.csv";

        File historyFile = new File(pathToHistoryFile);
        if (!historyFile.exists()) throw new UserOrHistoryNotFoundException();

        try (BufferedWriter writer = Files.newBufferedWriter(Path.of(pathToHistoryFile))) {
            writer.write("");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void cleanUserCacheFromMemoryByQuery(String id, String query) {
        if (cache.containsKey(id)) {
            cache.get(id).remove(query);
        }
    }

    public void cleanAllUserCacheFromMemory(String id) {
        cache.remove(id);
    }

    public void cleanAllUserData(String id) {
        cleanAllUserCacheFromMemory(id);
        deleteUserFolder(id);
    }

    public void deleteUserFolder(String id) {
        final String userFolderPath = bsa_giphy + "/users/" + id;

        File userFolder = new File(userFolderPath);
        try {
            FileUtils.deleteDirectory(userFolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void log(String query, String path, String userId) {
        final String historyLogPath = bsa_giphy + "/users/" + userId + "/" + "history.csv";

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = formatter.format(new Date());

        try (PrintWriter pw = new PrintWriter(new FileOutputStream(historyLogPath, true))) {
            String logLine = strDate + "," + query + "," + path + "\n";
            pw.append(logLine);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
