package com.bsa.giphy.logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class RequestLogger {
    @Value("${request-log.path}")
    private String path;

    public void log(HttpServletRequest req, String message) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = formatter.format(date);

        try (PrintWriter pw = new PrintWriter(new FileOutputStream(path, true))) {
            String logLine = strDate + "," + message + "," + req.getMethod() + "," + req.getRequestURI() + "\n";
            pw.append(logLine);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
