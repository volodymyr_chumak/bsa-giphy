package com.bsa.giphy.controller;

import com.bsa.giphy.service.GifService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/gifs")
public class GifController {
    private final GifService gifService;

    @Autowired
    public GifController(GifService gifService) {
        this.gifService = gifService;
    }

    @GetMapping
    public List<String> getAllGifs() {
        return gifService.getPaths();
    }
}
