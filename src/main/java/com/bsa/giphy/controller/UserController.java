package com.bsa.giphy.controller;

import com.bsa.giphy.dto.CacheDto;
import com.bsa.giphy.dto.HistoryDto;
import com.bsa.giphy.exception.GifNotFoundException;
import com.bsa.giphy.exception.IdIncorrectException;
import com.bsa.giphy.exception.UserNotFoundException;
import com.bsa.giphy.exception.UserOrHistoryNotFoundException;
import com.bsa.giphy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}/all")
    public List<CacheDto> getAllFilesFromDrive(@PathVariable String id) {
        if (id.isBlank()) throw new IdIncorrectException();

        Optional<List<CacheDto>> cache = userService.getAllFiles(id);

        if (cache.isEmpty()) throw new UserNotFoundException();

        return cache.get();
    }

    @GetMapping("/{id}/history")
    public List<HistoryDto> getHistory(@PathVariable String id) {
        if (id.isBlank()) throw new IdIncorrectException();

        Optional<List<HistoryDto>> history = userService.getHistory(id);

        if (history.isEmpty()) throw new UserOrHistoryNotFoundException();

        return history.get();
    }

    @DeleteMapping("/{id}/history/clean")
    public ResponseEntity<String> cleanHistory(@PathVariable String id) {
        if (id.isBlank()) throw new IdIncorrectException();

        try {
            userService.cleanHistory(id);
        } catch (UserOrHistoryNotFoundException ex) {
            throw new UserOrHistoryNotFoundException();
        }

        return new ResponseEntity<>("History cleared!", HttpStatus.OK);
    }

    @GetMapping("/{id}/search")
    public String searchGif(@PathVariable String id, @RequestParam String query, @RequestParam(required = false) String force) {
        if (id.isBlank()) throw new IdIncorrectException();

        String forceValue = force != null ? "true" : "false";
        Optional<String> path = userService.searchGif(id, query, forceValue);
        if (path.isEmpty()) throw new GifNotFoundException();

        return path.get();
    }

    @PostMapping("/{id}/generate")
    public String generateGif(@PathVariable String id, @RequestParam String query, @RequestParam(required = false) String force) {
        if (id.isBlank()) throw new IdIncorrectException();

        String forceValue = force != null ? "true" : "false";
        Optional<String> gif = userService.generateGif(id, query, forceValue);
        if (gif.isEmpty()) throw new GifNotFoundException();

        return gif.get();
    }

    @DeleteMapping("/{id}/reset")
    public ResponseEntity<String> cleanCacheFromRam(@PathVariable String id, @RequestParam(required = false) String query) {
        if (id.isBlank()) throw new IdIncorrectException();

        if (query != null) {
            userService.cleanUserCacheFromMemoryByQuery(id, query);
        } else {
            userService.cleanAllUserCacheFromMemory(id);
        }

        return new ResponseEntity<>("Cache cleared!", HttpStatus.OK);
    }

    @DeleteMapping("/{id}/clean")
    public ResponseEntity<String> cleanAllData(@PathVariable String id) {
        if (id.isBlank()) throw new IdIncorrectException();

        userService.cleanAllUserData(id);

        return new ResponseEntity<>("Cache cleared!", HttpStatus.OK);
    }
}
