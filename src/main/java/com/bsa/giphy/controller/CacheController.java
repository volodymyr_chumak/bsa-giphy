package com.bsa.giphy.controller;

import com.bsa.giphy.dto.CacheDto;
import com.bsa.giphy.service.CacheService;
import com.bsa.giphy.service.HttpGiphyApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/cache")
public class CacheController {
    private final CacheService cacheService;

    @Autowired
    public CacheController(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @GetMapping
    public List<CacheDto> getCacheFromDrive(@RequestParam(required = false) String query) {
        return cacheService.getFromDrive(query);
    }

    @PostMapping("/generate")
    public CacheDto generateGif(@RequestParam String query) {
        return cacheService.downloadGif(query);
    }

    @DeleteMapping
    public ResponseEntity<String> delete() {
        cacheService.cleanCacheFolder();
        return new ResponseEntity<>("Cache cleared!", HttpStatus.OK);
    }
}
