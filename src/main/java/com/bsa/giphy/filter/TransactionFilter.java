package com.bsa.giphy.filter;

import com.bsa.giphy.logger.RequestLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
@Order(1)
public class TransactionFilter implements Filter {

    private final RequestLogger logger;

    @Autowired
    public TransactionFilter(RequestLogger logger) {
        this.logger = logger;
    }

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        logger.log(req,  "Starting a transaction for req : ");

        chain.doFilter(request, response);
        logger.log(req,  "Committing a transaction for req : ");
    }
}