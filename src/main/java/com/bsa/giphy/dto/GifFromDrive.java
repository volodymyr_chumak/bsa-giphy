package com.bsa.giphy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GifFromDrive {
    private String id;
    private String path;
}
