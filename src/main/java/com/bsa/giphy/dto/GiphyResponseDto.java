package com.bsa.giphy.dto;

import lombok.Data;

@Data
public class GiphyResponseDto {
    private String query;
}
