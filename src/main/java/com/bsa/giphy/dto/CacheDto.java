package com.bsa.giphy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class CacheDto {
    private String query;
    private List<String> gifs;
}
