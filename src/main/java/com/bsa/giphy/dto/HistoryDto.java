package com.bsa.giphy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HistoryDto {
    private String date;
    private String query;
    private String gif;
}
