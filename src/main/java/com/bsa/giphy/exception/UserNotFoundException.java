package com.bsa.giphy.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "User don`t exist")
public class UserNotFoundException extends RuntimeException {
}
