package com.bsa.giphy.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Id incorrect!")
public class IdIncorrectException extends RuntimeException {
}
